//
//  Operacion.swift
//  Calculadora
//
//  Created by Coordinador on 8/2/20.
//  Copyright © 2020 Academia Moviles. All rights reserved.
//

import Foundation

enum Operacion: String{
    case suma = "+"
    case resta = "-"
    case multiplicacion = "*"
    case division = "/"
    case raizCuadrada = "√"
    
    func puedoOperar(numeroA: Double, numeroB: Double) -> Bool{
        switch self {
        case .division:
            return numeroB != 0
        case .raizCuadrada:
            return numeroA >= 0
        default:
            return true
        }
    }
    
    func operar(numeroA a:Double, numeroB b:Double) -> Double{
        switch self {
        case .suma:
            return a + b
        case .resta:
            return a - b
        case .multiplicacion:
            return a * b
        case .division:
            return a / b
        case .raizCuadrada:
            return a.squareRoot()
        }
    }
}
