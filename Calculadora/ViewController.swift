//
//  ViewController.swift
//  Calculadora
//
//  Created by Coordinador on 8/1/20.
//  Copyright © 2020 Academia Moviles. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    // MARK: - Propiedades
    // <-- Propiedad obligatoria/requerida nace NIL, pero no es optional
    //var nombre: String!
    
    @IBOutlet weak var resultadoLabel: UILabel!
    @IBOutlet weak var igualButton: UIButton!
    var numeroAlmacenado: Double = 0
    var operador: Operacion?
    var contadorPuntos: Int = 0
    var seRealizoOperacion: Bool = false
    
    // MARK: - ciclo de vida de un ViewController
    override func viewDidLoad() {
        super.viewDidLoad()
        resultadoLabel.text = "0"
    }

    @IBAction func botonNumericoTapped(_ sender: UIButton) {
        if resultadoLabel.text  == "0"{
            resultadoLabel.text = sender.titleLabel?.text
        }else if seRealizoOperacion == false {
            let numero = resultadoLabel.text! + sender.titleLabel!.text!
            resultadoLabel.text = numero
        }else{
            resultadoLabel.text = sender.titleLabel!.text!
            seRealizoOperacion = false
        }
        
    }
    
    @IBAction func operacionButtonTapped(_ sender: UIButton) {
        guard let numeroStr = resultadoLabel.text,
              let numero = Double(numeroStr),
            let simbolo = sender.titleLabel?.text else {
            return
        }
        operador = Operacion(rawValue: simbolo)
        numeroAlmacenado = numero
        resultadoLabel.text = "0"
        contadorPuntos = 0
        seRealizoOperacion = false
    }
    
    @IBAction func igualButtonTapped(_ sender: UIButton) {
        guard let operacion = operador,
              let numeroStr = resultadoLabel.text,
              let numero2 = Double(numeroStr)else {
                return
        }
        if operacion.puedoOperar(numeroA: numeroAlmacenado, numeroB: numero2){
            let resultado = operacion.operar(numeroA: numeroAlmacenado, numeroB: numero2)
            resultadoLabel.text = "\(resultado)"
            numeroAlmacenado = 0
            contadorPuntos = 0
            seRealizoOperacion = true
        }
    }
    
    @IBAction func puntoDecimalButtonTapped(_ sender: UIButton) {
        if contadorPuntos > 0{
            return
        }
        if seRealizoOperacion {
            return
        }
        resultadoLabel.text = resultadoLabel!.text! + sender.titleLabel!.text!
        contadorPuntos += 1
        
    }
    
    @IBAction func limpiaButtonTapped(_ sender: UIButton) {
        resultadoLabel.text = "0"
        numeroAlmacenado = 0
        contadorPuntos = 0
        seRealizoOperacion = false
    }
    
}

